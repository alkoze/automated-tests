Unit testides vaatame, kuidas kood teeb põhiklassi ***WholeWeather***. Et kui on mingi viga, siis programm näitab millega viga on seotud, et vajalikud andmed on loodud ja salvestatud õigesti.

Inegration testides on testitud kuidas ***WeatherApi*** töötab välise API'ga. On testitud millist infot projekti api saab internetist, nagu õnnestuse või ebaõnnestuse koodid, et on olemas ***temp*** field j.n.e.

Mock testides vaadatakse, kuidas programm töötab ilma välise api andmeteta.

UI testides kontrollitakse, et otsitud info on olemas veebileheküljel. UI testide jaoks on tehtud eraldi package ***PageObject*** kus on toodud kõik klassid testimiseks.

Et rakendust käivitada on vaja kõigipealt failisse ***input.txt*** kirjutada kõike linnu mille jaoks soovite forecast'i saada. Iga linna on vaja kirjutada uuel real.
Seejärel on vaja klassis ***SetWeatherReport*** käivitada ***main*** meetodit. Oodatud tulemus tuleb packagi`sse ***output*** ning faili kuju on ***linnaNimi.txt***.

Et käivitada integratsioon teste on väja käivitada klassi ***OpenWeatherMapTest***, UI testideks käivitada klassi ***UserInterfaceTests***, 
Unit testideks käivitada klassi ***UnitTests***, Mock testide jaoks käivitada klassi ***WeatherWiseTest***.