package gm.taltech.ee.weatherwise.api.data_holders;

import gm.taltech.ee.weatherwise.api.dto.ReportDto;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastReport {
    private ArrayList<ReportDto> forecast;
}
