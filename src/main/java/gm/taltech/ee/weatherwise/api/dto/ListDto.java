package gm.taltech.ee.weatherwise.api.dto;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListDto {
    private Long dt;
    private MainDto main;
    private String dt_txt;
}
