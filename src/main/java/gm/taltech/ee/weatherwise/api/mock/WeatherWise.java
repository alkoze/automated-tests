package gm.taltech.ee.weatherwise.api.mock;

import gm.taltech.ee.weatherwise.api.WeatherApi;
import gm.taltech.ee.weatherwise.api.data_holders.WeatherReport;
import gm.taltech.ee.weatherwise.api.data_holders.WeatherReportDetails;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;

public class WeatherWise {

    public   WeatherApi weatherApi;


    public WeatherWise(WeatherApi weatherApi) {
        this.weatherApi = weatherApi;
    }

    public WeatherReport getWeatherReportForCity(String city) throws Exception, CityNotFoundException, invalidApiKeyException {
        WeatherReport weatherReport = new WeatherReport();
        WeatherReportDetails weatherReportDetails = new WeatherReportDetails();
        if (city.isEmpty()) {
            throw new CityNotFoundException("City name is empty!");
        } else {
            CurrentWeatherData currentWeatherData = weatherApi.getCurrentWeatherDataForCity(city);
            if (currentWeatherData != null) {
                weatherReportDetails.setCity(currentWeatherData.getName());
                weatherReport.setWeatherReportDetails(weatherReportDetails);
                return weatherReport;
            } else {
                throw new Exception("Current weather data missing");
            }
        }
    }
}
