package gm.taltech.ee.weatherwise.api.response;

import gm.taltech.ee.weatherwise.api.dto.ListDto;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FutureWeatherData {
    private ArrayList<ListDto> list;
}
