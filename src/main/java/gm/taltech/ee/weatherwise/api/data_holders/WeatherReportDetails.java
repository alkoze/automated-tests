package gm.taltech.ee.weatherwise.api.data_holders;

import lombok.Data;

@Data
public class WeatherReportDetails {
    private String city;
    private String coordinates;
    private String temperatureUnit;
}
