package gm.taltech.ee.weatherwise.api.dto;

import lombok.Data;

@Data
public class CoordinatesDto {
    private Double lon;
    private Double lat;
}

