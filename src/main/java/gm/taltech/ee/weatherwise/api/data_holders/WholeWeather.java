package gm.taltech.ee.weatherwise.api.data_holders;

import gm.taltech.ee.weatherwise.api.data_holders.CurrentWeatherReport;
import gm.taltech.ee.weatherwise.api.data_holders.ForecastReport;
import gm.taltech.ee.weatherwise.api.data_holders.WeatherReportDetails;
import lombok.Data;

@Data
public class WholeWeather {
    private WeatherReportDetails weatherReportDetails;
    private CurrentWeatherReport currentWeatherReport;
    private ForecastReport forecastReport;
}
