package gm.taltech.ee.weatherwise.api.data_holders;


import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentWeatherReport {
    private Double temperature;
    private Integer humidity;
    private Integer pressure;
}
