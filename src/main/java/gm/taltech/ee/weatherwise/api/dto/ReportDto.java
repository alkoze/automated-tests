package gm.taltech.ee.weatherwise.api.dto;

import gm.taltech.ee.weatherwise.api.data_holders.CurrentWeatherReport;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportDto {
    private CurrentWeatherReport futureWeatherReport;
    private String date;
}
