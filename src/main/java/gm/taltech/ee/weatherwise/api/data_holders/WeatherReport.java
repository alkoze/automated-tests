package gm.taltech.ee.weatherwise.api.data_holders;

import lombok.Data;

@Data
public class WeatherReport {
    private WeatherReportDetails weatherReportDetails;
}
