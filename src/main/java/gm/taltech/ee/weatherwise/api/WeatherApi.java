package gm.taltech.ee.weatherwise.api;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.api.response.FutureWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNameIsEmptyException;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;

import static com.sun.jersey.api.client.Client.create;
import static com.sun.jersey.api.json.JSONConfiguration.FEATURE_POJO_MAPPING;
import static java.lang.Boolean.TRUE;

public class WeatherApi {
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5";
    private static final String APPID = "6ac8c9e4cc6c9ed16d0a5d8772ac8ea0";
    private static Client client = getConfiguredClient();

    public CurrentWeatherData getCurrentWeatherDataForCity(String cityName) throws CityNameIsEmptyException, CityNotFoundException, invalidApiKeyException {
        throwExceptions(getClientResponse(cityName, "/weather").getEntity(CurrentWeatherData.class));
        return getClientResponse(cityName, "/weather").getEntity(CurrentWeatherData.class);
    }

    public FutureWeatherData getFutureWeatherDataForCity(String cityName) throws CityNameIsEmptyException, CityNotFoundException, invalidApiKeyException {
        throwExceptions(getClientResponse(cityName, "/weather").getEntity(CurrentWeatherData.class));
        return getClientResponse(cityName, "/forecast").getEntity(FutureWeatherData.class);
    }

    private void throwExceptions(CurrentWeatherData currentWeatherData) throws CityNameIsEmptyException, CityNotFoundException, invalidApiKeyException {
        if (currentWeatherData.getCod().equals(401)){
            throw new invalidApiKeyException("API key is not correct or not activated!");
        }
        if (currentWeatherData.getCod().equals(400)){
            throw new CityNameIsEmptyException("City name is required!");
        }
        if (currentWeatherData.getCod().equals(404)){
            throw new CityNotFoundException("City not found");
        }
    }

    private ClientResponse getClientResponse(String cityName, String urlAddition) {
        String resourceUrl = BASE_URL + urlAddition;
        return client.resource(resourceUrl)
                .queryParam("q", cityName)
                .queryParam("APPID", APPID)
                .queryParam("units", "metric")
                .get(ClientResponse.class);
    }

    private static Client getConfiguredClient() {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJaxbJsonProvider.class);
        config.getFeatures().put(FEATURE_POJO_MAPPING, TRUE);
        return create(config);
    }

    public static void main(String[] args) throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        WeatherApi api = new WeatherApi();
        System.out.println(api.getCurrentWeatherDataForCity("a"));

    }
}
