package gm.taltech.ee.weatherwise.exception;

public class CityNotFoundException extends Throwable {
    public CityNotFoundException(String s) {
        super(s);
    }
}
