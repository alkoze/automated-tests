package gm.taltech.ee.weatherwise.exception;

public class CityNameIsEmptyException extends Exception{

    public CityNameIsEmptyException(String errorMessage) {
        super(errorMessage);
    }
}
