package gm.taltech.ee.weatherwise;

import gm.taltech.ee.weatherwise.api.WeatherApi;
import gm.taltech.ee.weatherwise.api.data_holders.CurrentWeatherReport;
import gm.taltech.ee.weatherwise.api.data_holders.ForecastReport;
import gm.taltech.ee.weatherwise.api.data_holders.WeatherReportDetails;
import gm.taltech.ee.weatherwise.api.data_holders.WholeWeather;
import gm.taltech.ee.weatherwise.api.dto.ReportDto;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.api.response.FutureWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNameIsEmptyException;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.util.Precision;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class SetWeatherReport {


    public static void main(String[] args) throws IOException, CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        LinkedList<String> cities = readCitiesFromFile();
        for (String city : cities){
            SetWeatherReport weatherData = new SetWeatherReport(city);
            writeWeatherDataToFile(String.format("%s.json", city.substring(0,1).toUpperCase()+city.substring(1)), weatherData.setWholeWeather());
        }
    }

    private final String cityName;
    private WeatherApi api = new WeatherApi();
    private WholeWeather wholeWeather = new WholeWeather();

    public WholeWeather getWholeWeather (){
        return wholeWeather;
    }

    public SetWeatherReport(String cityName){
        this.cityName = cityName;
    }

    public String setWholeWeather() throws IOException, CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        ObjectWriter writer = new ObjectMapper().writer().withDefaultPrettyPrinter();
        setCurrentData();
        setWeatherReportDetails();
        setFutureWeather();
        return writer.writeValueAsString(wholeWeather);
    }

    private void setCurrentData() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        CurrentWeatherReport currentWeatherReport = new CurrentWeatherReport();
        CurrentWeatherData currentWeatherData = api.getCurrentWeatherDataForCity(cityName);
        currentWeatherReport.setTemperature(currentWeatherData.getMain().getTemp());
        currentWeatherReport.setHumidity(currentWeatherData.getMain().getHumidity());
        currentWeatherReport.setPressure(currentWeatherData.getMain().getPressure());
        wholeWeather.setCurrentWeatherReport(currentWeatherReport);
    }
    private void setWeatherReportDetails() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        WeatherReportDetails weatherReportDetails = new WeatherReportDetails();
        CurrentWeatherData currentWeatherData = api.getCurrentWeatherDataForCity(cityName);
        weatherReportDetails.setCity(currentWeatherData.getName());
        weatherReportDetails.setCoordinates(currentWeatherData.getCoord().getLat().toString() +", " + currentWeatherData.getCoord().getLon().toString());
        weatherReportDetails.setTemperatureUnit("Celsius");
        wholeWeather.setWeatherReportDetails(weatherReportDetails);
    }
    private void setFutureWeather() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        FutureWeatherData futureWeatherData = api.getFutureWeatherDataForCity(cityName);
        ForecastReport forecastReport = new ForecastReport();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        forecastReport.setForecast(new ArrayList<>());
        Date date = new Date();
        double temp = 0;
        int humidity = 0;
        int pressure = 0;
        int sameDate = 0;
        String previousForecastDate = futureWeatherData.getList().get(0).getDt_txt().split(" ")[0];
        for (int i = 0; i < futureWeatherData.getList().size(); i++) {
            String forecastDate = futureWeatherData.getList().get(i).getDt_txt().split(" ")[0];
            if (forecastDate.equals(formatter.format(date))){
                previousForecastDate = futureWeatherData.getList().get(i+1).getDt_txt().split(" ")[0];
                continue;
            }
            if (!(forecastDate.equals(previousForecastDate))){
                if (forecastReport.getForecast().size() == 3)
                    break;
                CurrentWeatherReport currentWeatherReport = new CurrentWeatherReport();
                ReportDto reportDto = new ReportDto();
                temp = temp/sameDate;
                humidity = humidity/sameDate;
                pressure = pressure/sameDate;
                currentWeatherReport.setTemperature(Precision.round(temp, 2));
                currentWeatherReport.setHumidity(humidity);
                currentWeatherReport.setPressure(pressure);
                reportDto.setDate(previousForecastDate);
                reportDto.setFutureWeatherReport(currentWeatherReport);
                forecastReport.getForecast().add(reportDto);
                temp = 0;
                humidity = 0;
                pressure = 0;
                sameDate = 0;
            } else {
                temp += futureWeatherData.getList().get(i).getMain().getTemp();
                humidity += futureWeatherData.getList().get(i).getMain().getHumidity();
                pressure += futureWeatherData.getList().get(i).getMain().getPressure();
                sameDate ++;
            }
            previousForecastDate = forecastDate;
        }
        wholeWeather.setForecastReport(forecastReport);
    }
    private static LinkedList<String> readCitiesFromFile() throws IOException{
        FileInputStream fis = new FileInputStream("src/main/java/gm/taltech/ee/weatherwise/input/input.txt");
        String data = IOUtils.toString(fis, "UTF-8");
        LinkedList<String> cities = new LinkedList<>();
        for(String city : data.split("\n")){
            System.out.println(city);
            if (!city.isEmpty()){
                city = city.trim();
                cities.add(city);
            }
        }
        return cities;
    }

    private static void writeWeatherDataToFile(String fileName, String wholeReport) throws IOException {
        FileWriter fileWriter = new FileWriter("src/main/java/gm/taltech/ee/weatherwise/output/" + fileName);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(wholeReport);
        printWriter.close();
    }
}
