package gm.taltech.ee.UiTests.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class FindMummaEmail {

    private By employeeName = By.xpath("//INPUT[@id='search-text']");
    private By submitButton = By.xpath("//BUTTON[@class='gradient search-trigger']");
    private By mummaEmail = By.xpath("//table[@class='pt']");
    private By emplyeeOption = By.xpath("(//LABEL)[2]");
    private WebDriver driver;

    public FindMummaEmail(WebDriver driver) {
        this.driver = driver;
    }

    public void enterSearch(String search) {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(employeeName)).click().sendKeys(search).perform();
        action.moveToElement(driver.findElement(employeeName)).click().moveToElement(driver.findElement(emplyeeOption)).click().perform();
        action.moveToElement(driver.findElement(employeeName)).click().moveToElement(driver.findElement(emplyeeOption)).click().perform();
        driver.findElement(submitButton).click();
    }

    public String getMummaEamil(){
        return driver.findElement(mummaEmail).getText();
    }

}
