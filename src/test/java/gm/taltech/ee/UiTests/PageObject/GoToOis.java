package gm.taltech.ee.UiTests.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.interactions.Actions;

public class GoToOis {

    private WebDriver driver;
    private By sisene = By.xpath("//a[@class='active_language_a gradient']");
    private By ois = By.linkText("ÕIS");

    public GoToOis(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOis() {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(sisene));
        action.perform();
        driver.findElement(sisene);
        driver.findElement(ois).click();
    }
}
