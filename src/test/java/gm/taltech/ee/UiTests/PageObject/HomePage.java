package gm.taltech.ee.UiTests.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;
    private By getTudeng = By.linkText("Tudeng");
    private By oppeinfo = By.linkText("Õppeinfo");


    public HomePage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickTudeng() {
        driver.findElement(getTudeng).click();
    }

    public void clickoppeinfo() {
        driver.findElement(oppeinfo).click();
    }

}
