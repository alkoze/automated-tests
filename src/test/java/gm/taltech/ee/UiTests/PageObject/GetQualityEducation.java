package gm.taltech.ee.UiTests.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class GetQualityEducation {

    private WebDriver driver;
    private By QualityEducation = By.tagName("h2");

    public GetQualityEducation(WebDriver driver) {
        this.driver = driver;
    }

    public String FindInformation() {
        return driver.findElement(QualityEducation).getText();
    }

}
