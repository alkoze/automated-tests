package gm.taltech.ee.UiTests;


import gm.taltech.ee.UiTests.PageObject.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class UserInterfaceTests {
    private WebDriver driver;
    private HomePage homePage;

    @BeforeClass
    public void set_up_driver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
    }

    @BeforeMethod
    public void open_driver() {
        driver.get("https://www.ttu.ee/et/");
    }

    @Test
    public void should_find_kvaliteetne_haridus_text() {
        GetQualityEducation info = new GetQualityEducation(driver);
        homePage.clickTudeng();
        homePage.clickoppeinfo();
        String education_info = "Kvaliteetne haridus";
        Assert.assertEquals(education_info, info.FindInformation());
    }

    @Test
    public void should_find_german_mumma_email() {
        FindMummaEmail findMummaEmail = new FindMummaEmail(driver);
        findMummaEmail.enterSearch("German Mumma");
        Assert.assertTrue(findMummaEmail.getMummaEamil().contains("german.mumma@taltech.ee"));
    }

    @Test public void must_go_to_ois(){
        GoToOis goToOis = new GoToOis(driver);
        goToOis.clickOis();
        Assert.assertEquals("https://ois2.ttu.ee/uusois/uus_ois2.tud_leht", driver.getCurrentUrl());
    }

    @AfterClass
    public void close_driver() {
        if (driver != null) {
            driver.close();
        }
    }
}
