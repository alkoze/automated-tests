package gm.taltech.ee.weatherwise;

import gm.taltech.ee.weatherwise.api.WeatherApi;
import gm.taltech.ee.weatherwise.api.data_holders.WeatherReport;
import gm.taltech.ee.weatherwise.api.mock.WeatherWise;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.CityNameIsEmptyException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WeatherWiseTest {

    @Mock
    private WeatherApi weatherApiMock;
    private WeatherWise weatherWise;

    @Before
    public void setup(){
        weatherWise = new WeatherWise(weatherApiMock);
    }

    @Test
    public void should_return_weather_report_for_city() throws Exception, CityNotFoundException, invalidApiKeyException {
        String city ="Tallinn";
        CurrentWeatherData currentWeatherData = new CurrentWeatherData();
        currentWeatherData.setName(city);
        Mockito.when(weatherApiMock.getCurrentWeatherDataForCity(anyString())).thenReturn(currentWeatherData);
        WeatherReport weatherReport = weatherWise.getWeatherReportForCity(city);
        assertEquals(weatherReport.getWeatherReportDetails().getCity(), city);
    }

    @Test
    public void should_call_weather_api_if_city_is_not_missing() throws CityNotFoundException, Exception, invalidApiKeyException {
        String city ="Tallinn";
        CurrentWeatherData currentWeatherData = new CurrentWeatherData();
        currentWeatherData.setName(city);
        Mockito.when(weatherApiMock.getCurrentWeatherDataForCity(anyString())).thenReturn(currentWeatherData);
        weatherWise.getWeatherReportForCity(city);
        verify(weatherWise.weatherApi).getCurrentWeatherDataForCity(city);
    }

}