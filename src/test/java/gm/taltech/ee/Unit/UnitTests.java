package gm.taltech.ee.Unit;

import gm.taltech.ee.weatherwise.SetWeatherReport;
import gm.taltech.ee.weatherwise.api.WeatherApi;
import gm.taltech.ee.weatherwise.api.data_holders.WholeWeather;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNameIsEmptyException;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;

public class UnitTests {
    @Test (expected = CityNotFoundException.class)
    public void should_return_error_when_city_is_unknown() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        //ARRANGE
        WeatherApi api = new WeatherApi();
        String city = "UnknownCity";
        //ACT
        api.getCurrentWeatherDataForCity(city);
    }

    @Test
    public void weather_report_should_return_correct_coordinates_of_given_city() throws CityNameIsEmptyException, CityNotFoundException, invalidApiKeyException, IOException {
        //ARRANGE
        SetWeatherReport setWeatherReport = new SetWeatherReport("Parnu");
        WholeWeather wholeWeather = setWeatherReport.getWholeWeather();
        //ACT
        setWeatherReport.setWholeWeather();
        //ASSERT
        assertEquals("58.39, 24.5", wholeWeather.getWeatherReportDetails().getCoordinates());
    }

    @Test
    public void weather_report_should_contain_given_city() throws CityNameIsEmptyException, IOException, CityNotFoundException, invalidApiKeyException {
        //ARRANGE
        SetWeatherReport setWeatherReport = new SetWeatherReport("Rakvere");
        WholeWeather wholeWeather = setWeatherReport.getWholeWeather();
        //ACT
        setWeatherReport.setWholeWeather();
        //ASSERT
        assertEquals("Rakvere", wholeWeather.getWeatherReportDetails().getCity());
    }

    @Test
    public void weather_report_should_contain_data_for_three_future_days() throws CityNameIsEmptyException, CityNotFoundException, IOException, invalidApiKeyException {
        //ARRANGE
        SetWeatherReport setWeatherReport = new SetWeatherReport("Parnu");
        WholeWeather wholeWeather = setWeatherReport.getWholeWeather();
        //ACT
        setWeatherReport.setWholeWeather();
        //ASSERT
        assertEquals(3, wholeWeather.getForecastReport().getForecast().size());
    }


}
