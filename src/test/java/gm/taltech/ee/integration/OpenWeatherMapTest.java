package gm.taltech.ee.integration;
import gm.taltech.ee.weatherwise.api.WeatherApi;
import gm.taltech.ee.weatherwise.api.response.CurrentWeatherData;
import gm.taltech.ee.weatherwise.exception.CityNameIsEmptyException;
import gm.taltech.ee.weatherwise.exception.CityNotFoundException;
import gm.taltech.ee.weatherwise.exception.invalidApiKeyException;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OpenWeatherMapTest {

    @Test
    public void should_return_status_code_200() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        WeatherApi myApi = new WeatherApi();
        CurrentWeatherData currentWeatherData = myApi.getCurrentWeatherDataForCity("Tartu");
        assertEquals("200", currentWeatherData.getCod().toString());
    }

    @Test (expected=CityNotFoundException.class)
    public void should_return_error() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        WeatherApi myApi = new WeatherApi();
        CurrentWeatherData currentWeatherData = myApi.getCurrentWeatherDataForCity("Таллинн");
        assertEquals("404", currentWeatherData.getCod().toString());
        assertEquals("city not found", currentWeatherData.getMessage());
    }

    @Test
    public void should_find_temperature_attribute() throws CityNotFoundException, CityNameIsEmptyException, invalidApiKeyException {
        WeatherApi myApi = new WeatherApi();
        CurrentWeatherData currentWeatherData = myApi.getCurrentWeatherDataForCity("Tartu");
        assertTrue(currentWeatherData.getMain().toString().contains("temp"));
    }
}
